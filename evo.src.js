//==============================================================================
// ==ClosureCompiler==
// @output_file_name evo.js
// @compilation_level ADVANCED_OPTIMIZATIONS
// ==/ClosureCompiler==
//==============================================================================
(function() {
    
if (!document.createElement('canvas').getContext) {
    console.log('Your browser is too old and doesn\'t support HTML5 canvas');
}

if (!document.getElementById('cvs-view')) {
    console.log('Something went wrong, please reload the page in some minutes.'
		+ ' If the problem is not solved please contact me.');
}

var container	      = document.getElementById('container');

var canvas	      = document.getElementById('cvs-view');
var ctx		      = canvas.getContext('2d');
var cvWidth	      = canvas.width  = 960;
var cvHeight	      = canvas.height = 520;
var cvFullscreen      = false;
var cvFramerate	      = 1e3/60;
var cvActualFramerate = 0;
var cvRenderPause     = false;
var cvRenderFunc      = function() {};

var cvRenderFrame     = (function() {
    return window['requestAnimationFrame']  || 
	   window['webkitRequestAnimationFrame'] || 
	   window['mozRequestAnimationFrame']    || 
	   window['oRequestAnimationFrame']      || 
	   window['msRequestAnimationFrame']     || 
	   function(callback) {
	       window.setTimeout(callback, actualFramerate);
	   };
})();

function cvSetFullscreen(fs) {
    cvFullscreen = fs;
    
    if (fs) {
        container.setAttribute('class', 'fs-container');
        cvUpdateSize();
    }
    else {
        container.setAttribute('class', 'window-container');
        cvWidth  = canvas.width  = 960;
        cvHeight = canvas.height = 520;
    }
}

function cvUpdateSize() {
    if (!cvFullscreen) return;
    
    cvWidth = window.innerWidth;
    canvas.width = cvWidth;
    
    cvHeight = window.innerHeight;
    canvas.height = cvHeight;
}

function cvDraw() {
    var startTime = new Date().getTime();
    cvRenderFunc();
    
    cvActualFramerate = new Date().getTime() - startTime;
    if (cvActualFramerate <= cvFramerate) {
        cvActualFramerate = cvFramerate;
    }
    
    if (!cvRenderPause) cvRenderFrame(cvDraw);
}


window.addEventListener('resize', function(e) {
    cvUpdateSize();
}, false);

document.addEventListener('webkitvisibilitychange', function() {
    var hidden = document['webkitHidden'];
    if (hidden) {
		cvRenderPause = true;
    }
    else {
		cvRenderPause = false;
		cvDraw();
    }

    visibilityEvent(hidden);
}, false);


/**
 * Utility class
 * @constructor
 */
function clock() {
	this._startTime = new Date().getTime();
}

clock.prototype.getTime = function() {
	return new Date().getTime() - this._startTime;
}


// Defines the number of lines that are drawn at each framee
var divider    = 10;

var mTimer     = new clock();
var mTimeDelay = 5e3;
var mDivTime   = mTimeDelay / divider;

var mImgLoaded = false;
var mImg       = document.createElement('img');
mImg.onload    = function() {
    mImgLoaded = true;
};
mImg.src       = 'overlay.png';

// It's either I don't get it right or that when I try to add white shadows to
// make a blurry effect, they don't appear =/
function mUpdate() {
    var mHeight3  = cvHeight * 1.5,
        mHeight2  = cvHeight * 1.25,
        mWidth_4  = cvWidth / 3,
        mWidth3_4 = 2 * cvWidth / 3;
    
    ctx.globalAlpha = .3;
    ctx.fillStyle = '#FC0'; // Background color, orange here
    ctx.fillRect(0, 0, cvWidth, cvHeight);
    
    ctx.globalAlpha = 1;
    var currentTimer = mTimer.getTime();
    
    ctx.lineWidth = (cvHeight + cvWidth) / 400;
    if (ctx.lineWidth < 1) ctx.lineWidth = 1;
    
    ctx.shadowBlur = 104;
    ctx.strokeStyle = '#FFF';
    for(var i = 0; i < divider; ++i) {
        var factor = ((currentTimer + mDivTime * i) % mTimeDelay)/mTimeDelay;
        var controller2y = cvHeight - factor * cvHeight;
        var controller1y = mHeight2 - factor * mHeight3;

        ctx.beginPath();
        ctx.moveTo(0, controller1y);
        ctx.bezierCurveTo(
            mWidth_4,  controller2y,
            mWidth3_4, controller2y,
            cvWidth,   controller1y
        );
        ctx.stroke();
    }
    
    ctx.shadowBlur = 0;
    if (mImgLoaded) {
        ctx.drawImage(mImg, 0, 0, 256, 256, 0, 0, cvWidth, cvHeight);
    }
}

document.body.style.backgroundColor = '#FC0';

cvSetFullscreen(true);
cvRenderFunc = mUpdate;
cvDraw();

})();
